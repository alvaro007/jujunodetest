var express = require('express');
var app = express();
var config = require('./config/config.js');

app.get('/', function (req, res) {
  res.send('Hello World from JujuNodeTest!');
});

var server = app.listen(config.listen_port, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});
